export default [
  {
    path: '/beers',
    name: 'beers',
    meta: {
      loadState: true,
      auth: false,
      isAuthPage: false,
      permissions: [
        /*
        // TODO: Example permission
        {
        subject: 'beer',
        actions: ['view']
        }
        */
      ]
    },
    // route level code-splitting
    component: () => import(/* webpackChunkName: "users" */ '../views/beers/Index')
  }
];
