export default [
  {
    path: '/users',
    name: 'users',
    meta: {
      loadState: true,
      auth: true,
      isAuthPage: false,
      permissions: [{
        subject: 'user',
        actions: ['view-nav-link', 'view']
      }]
    },
    // route level code-splitting
    component: () => import(/* webpackChunkName: "users" */ '../views/users/Index')
  },
  {
    path: '/users/:id',
    name: 'user-details',
    meta: {
      loadState: true,
      auth: true,
      isAuthPage: false,
      permissions: [{
        subject: 'user',
        actions: ['view']
      }]
    },
    // route level code-splitting
    component: () => import(/* webpackChunkName: "user-details" */ '../views/users/Single'),
  },
  {
    path: '/users/:id/settings',
    name: 'settings',
    meta: {
      loadState: true,
      auth: true,
      isAuthPage: false,
      permissions: [{
        subject: 'user',
        actions: ['see-profile-settings']
      }]
    },
    // route level code-splitting
    component: () => import(/* webpackChunkName: "user-settings" */ '../views/users/Settings'),
  }
];
