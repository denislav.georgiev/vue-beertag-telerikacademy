import Vue from 'vue';
import Vuex from 'vuex';
import initialState from './store/initialState';
import alertsModule from './store/modules/alerts';
import authModule from './store/modules/auth';
import localeModule from './store/modules/locale';
import usersModule from './store/modules/users';
import beersModule from './store/modules/beers';
import { localStore, sessionStore } from './store/persistentStore';

Vue.use(Vuex);

const options = {
  modules: {
    alerts: alertsModule(initialState.alerts),
    auth: authModule(initialState.auth),
    locale: localeModule(initialState.locale),
    users: usersModule(initialState.users),
    beers: beersModule(initialState.beers)
  },
  plugins: [
    localStore.plugin,
    sessionStore.plugin
  ]
};

export default new Vuex.Store(options);
