import userData from './data/user';
import adminPermissions from './data/adminPermissions';
import timeout from './data/timeout';

export default {
  // eslint-disable-next-line no-unused-vars
  login(auth) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          data: {
            user: userData,
            permissions: adminPermissions
          },
          message: 'Success'
        });
      }, timeout);
    });
  },
  logout() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          data: {},
          message: 'Success'
        });
      }, timeout);
    });
  },
  // eslint-disable-next-line no-unused-vars
  register(user) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          data: {},
          message: 'Success'
        });
      }, timeout);
    });
  }
};
