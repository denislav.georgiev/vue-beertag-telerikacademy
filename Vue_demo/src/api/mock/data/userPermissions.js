export default [{
  subject: 'beers',
  actions: ['view', 'filter']
}, {
  subject: 'users',
  actions: ['view', 'filter']
}, {
  subject: 'settings',
  actions: ['view', 'edit']
}];
