export default {
  firstName: 'John',
  lastName: 'Doe',
  photoUrl: '/images/placeholder.png',
  role: 'administrator',
  ratedBeers: []
};
