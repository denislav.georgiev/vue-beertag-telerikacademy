export default [{
  subject: 'beers',
  actions: ['view', 'create', 'edit', 'remove', 'filter']
}, {
  subject: 'user',
  actions: ['view', 'create', 'edit', 'remove', 'filter']
}, {
  subject: 'settings',
  actions: ['view', 'edit']
}];
