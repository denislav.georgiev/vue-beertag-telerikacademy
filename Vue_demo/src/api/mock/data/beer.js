export default {
  name: 'Carlsberg',
  brewery: 'Carlsberg A/S',
  country: 'Denmark',
  abv: 5,
  description: 'From a bottle, pours as a mostly clear yellow with a fluffy off-white head that quickly dispersed; lots of carbonation. Light aroma of sweet grains and malt. Even balance of hops and sweet malt flavors; some tastes of bready yeast, pretty nice. But there are also off-putting metallic and corn flavors.',
  style: 'European Pale Lager',
  picture: 'https://cdn.beeradvocate.com/im/beers/2270.jpg'
};
