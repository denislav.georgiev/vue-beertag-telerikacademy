import axios from 'axios';
import { getToken, getAccessQuery, apiUrl } from '../../utils/serverApi';

export default {
  getCategories() {
    return axios({
      method: 'get',
      url: 'http://localhost/merchy/public/api/configs/countries'
      //  url: 'http://localhost:8084/api/beers'
    });
  },
  getBeers(queryFilters = {}) {
    const {
      page = 1,
      perPage = 15,
      order = 'asc',
      sort = '',
      search = '',
      brewery = '',
      country = '',
      abv = 0,
      style = ''
    } = queryFilters;

    return axios({
      method: 'get',
      params: {
        page,
        per_page: perPage,
        order,
        sort,
        search,
        brewery,
        country,
        abv,
        style
      },
      url: `${apiUrl}/beers`
    });
  },
  getBeer(id) {
    return axios({
      method: 'get',
      url: `${apiUrl}/beers/${id}`
    });
  },
  createBeer(beer) {
    const access_token = getToken();

    return axios({
      method: 'post',
      url: `${apiUrl}/beers`,
      data: {
        access_token,
        ...beer
      }
    });
  },
  updateBeer(beer) {
    const query = getAccessQuery();

    return axios({
      method: 'put',
      url: `${apiUrl}/beers/${beer.id}${query}`,
      data: {
        ...beer
      }
    });
  },
  removeBeer(id) {
    const query = getAccessQuery();

    return axios({
      method: 'delete',
      url: `${apiUrl}/beers/${id}${query}`
    });
  }
};
