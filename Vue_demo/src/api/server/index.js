import auth from './auth';
import users from './users';
import beers from './beers';

export default {
  auth,
  users,
  beers
};
