import axios from 'axios';
import { getToken, apiUrl } from '../../utils/serverApi';

export default {
  login(userAuth) {
    console.warn('login::');
    console.log(userAuth);
    return axios({
      method: 'post',
      url: `${apiUrl}/auth/login`,
      data: userAuth
    });
  },
  logout() {
    const access_token = getToken();

    return axios({
      method: 'post',
      url: `${apiUrl}/auth/logout`,
      data: {
        access_token
      }
    });
  },
  register(user) {
    return axios({
      method: 'post',
      url: `${apiUrl}/register/new/${user.token}`,
      data: {
        ...user
      }
    });
  }
};
