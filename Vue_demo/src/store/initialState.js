export default {
  auth: {
    user: {},
    permissions: [],
    redirects: {
      index: '/',
      unauthenticated: '/login',
      authenticated: '/'
    }
  },
  locale: {
    language: 'en'
  },
  users: {
    activeItem: {},
    items: [],
    itemsMeta: {}
  },
  beers: {
    activeItem: {},
    items: [],
    itemsMeta: {}
  },
  alerts: {
    info: [],
    success: [],
    warning: [],
    error: []
  }
};
