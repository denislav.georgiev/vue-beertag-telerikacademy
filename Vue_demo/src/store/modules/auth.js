// eslint-disable-next-line import/no-unresolved
import apiClient from 'api-client';
import router from '../../router';
import initialState from '../initialState';
import { isNum, isObj, isArr } from '../../utils';
import store from '../../store';
import isAuthorized from '../../utils/authorization';

const authApi = apiClient.auth;

/**
 * @description Is valid
 * @param auth
 * @returns {boolean}
 */
const isValid = (auth) => isObj(auth)
  && isObj(auth.user)
  && isArr(auth.permissions)
  && isObj(auth.redirects);

/**
 * @description Init state
 * @param initialState
 * @returns {*}
 */
const initState = initialState => {
  if (!isValid(initialState)) {
    throw Error('Invalid initial auth state');
  }

  const { user, permissions, redirects } = initialState;
  return {
    user,
    permissions,
    redirects
  };
};

/**
 * @description Getters
 * @type {*}
 */
export const getters = {
  user: ({ user }) => user,
  permissions: ({ permissions }) => permissions,
  redirects: ({ redirects }) => redirects,
  isAuthorized: ({ permissions }) => (requestedPermissions) => (
    isAuthorized(permissions, requestedPermissions)
  ),
  isLogged: ({ user }) => isNum(user.id)
};

/**
 * @description Handle alerts
 * @param data
 * @param alertType
 */
const handleAlerts = (data, alertType = 'error') => (
  store.dispatch('alerts/set', {
    data,
    alertType
  })
);

/**
 * @description Set ability
 * @param permissions
 */
const setAbility = (permissions) => {
  router.app.$ability.update(permissions);
};

/**
 * @description Revoke ability
 */
const revokeAbility = () => {
  router.app.$ability.update([]);
};

const actions = {
  login: ({ commit, state }, userAuth) => (
    authApi.login(userAuth).then((res) => {
      const { user, permissions } = res.data;
      const nextAuth = {
        ...state,
        user,
        permissions
      };

      setAbility(permissions);

      commit('SET', nextAuth);

      return router.replace({ name: 'index' });
    })
  ),
  logout: ({ commit }) => (
    authApi.logout().then((res) => {
      handleAlerts(res.data, 'success');
      revokeAbility();
      actions.resetAll({ commit });

      return router.replace({ name: 'index' });
    })
  ),
  register: (context, user) => (
    authApi.register(user).then((res) => {
      handleAlerts(res.data, 'success');
      return router.replace({ name: 'login' });
    })
  ),
  reset: ({ commit }) => (
    commit('SET', initialState.auth)
  ),
  resetAll: ({ commit }) => {
    actions.reset({ commit });
    store.dispatch('auth/reset');
    store.dispatch('users/reset');
    store.dispatch('beers/reset');
  },
  set: ({ commit }, auth) => {
    commit('SET', auth);
  }
};

const mutations = {
  SET(state, auth) {
    // eslint-disable-next-line no-param-reassign
    state.user = auth.user;
    // eslint-disable-next-line no-param-reassign
    state.permissions = auth.permissions;
    // eslint-disable-next-line no-param-reassign
    state.redirects = auth.redirects;
  }
};

export default initialState => ({
  namespaced: true,
  state: initState(initialState),
  getters,
  actions,
  mutations,
});
