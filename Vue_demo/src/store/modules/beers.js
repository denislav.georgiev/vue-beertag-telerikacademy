// eslint-disable-next-line import/no-unresolved
import apiClient from 'api-client';
import { isObj, isArr } from '../../utils';
import store from '../../store';
import initialState from '../initialState';
import beersServerApi from '../../api/server/beers';

const beersApi = apiClient.beers;


/**
 * @description Is valid
 * @param beers
 * @returns {boolean}
 */
const isValid = (beers) => {
  const { activeItem, items, itemsMeta } = beers;

  return isObj(activeItem)
    && isArr(items)
    && isObj(itemsMeta);
};

/**
 * @description Init state
 * @param initialState
 * @returns {*}
 */
const initState = initialState => {
  if (!isValid(initialState)) {
    throw Error('Invalid initial beers state');
  }

  const { activeItem, items, itemsMeta } = initialState;

  return {
    activeItem,
    items,
    itemsMeta
  };
};

/**
 * @description Getters
 * @type {*}
 */
export const getters = {
  activeItem: ({ activeItem }) => activeItem,
  items: ({ items }) => items,
  itemsMeta: ({ itemsMeta }) => itemsMeta
};

/**
 * @description Handle alerts
 * @param data
 * @param alertType
 */
const handleAlerts = (data, alertType = 'error') => (
  store.dispatch('alerts/set', {
    data,
    alertType
  })
);

const actions = {
  getCategories: () => (
    beersServerApi.getCategories()
  ),
  getBeers: ({ commit, state }, query) => (
    beersApi.getBeers(query).then((res) => {
      const nextItemsData = {
        items: res.data
      };

      const nextState = {
        ...state,
        ...nextItemsData
      };

      commit('SET', nextState);

      return nextItemsData;
    })
  ),
  getBeer: ({ commit, state }, query) => (
    beersApi.getBeer(query).then((res) => {
      const { activeItem } = state;
      const nextActiveItem = {
        ...activeItem,
        ...res.data
      };

      const nextState = {
        ...state,
        activeItem: nextActiveItem
      };

      commit('SET', nextState);

      return nextActiveItem;
    })
  ),
  createBeer: (context, user) => (
    beersApi.createBeer(user).then((res) => {
      const nextUser = res.data;

      handleAlerts({
        data: {
          message: 'Beer created successfully.'
        }
      }, 'success');

      return nextUser;
    })
  ),
  updateBeer: ({ commit, state }, user) => (
    beersApi.updateBeer(user).then((res) => {
      const { activeItem } = state;
      const nextUser = res.data;

      const nextState = {
        ...state,
        activeItem: {
          ...activeItem,
          ...nextUser
        }
      };

      commit('SET', nextState);

      handleAlerts({
        data: {
          message: 'Beer updated successfully.'
        }
      }, 'success');

      return nextUser;
    })
  ),
  removeBeer: (context, user) => (
    beersApi.removeBeer(user).then(() => (
      handleAlerts({
        data: {
          message: 'Beer removed successfully.'
        }
      }, 'success')
    ))
  ),
  setActiveBeer: ({ commit, state }, beer) => {
    const nextState = {
      ...state,
      activeItem: beer
    };

    return commit('SET', nextState);
  },
  reset: ({ commit }) => (
    commit('SET', initialState.beers)
  ),
  set: ({ commit }, beers) => {
    commit('SET', beers);
  }
};

const mutations = {
  SET(state, beers) {
    state.activeItem = beers.activeItem;
    state.items = beers.items;
    state.itemsMeta = beers.itemsMeta;
  }
};

export default initialState => ({
  namespaced: true,
  state: initState(initialState),
  getters,
  actions,
  mutations,
});
