import { headers } from './axios';

headers('Accept', 'application/json');
headers('Content-Type', 'application/x-www-form-urlencoded');

if (process.env.VUE_APP_API_USE_TOKEN) {
  headers('Authorization', `Basic ${process.env.VUE_APP_API_TOKEN}`);
}
