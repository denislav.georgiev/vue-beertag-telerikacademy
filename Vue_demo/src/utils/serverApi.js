import store from '../store';
import { hasProp } from './index';

/**
 * @description Api url
 * @type {string}
 */
export const apiUrl = `${process.env.VUE_APP_API}`;

/**
 * @description Get token
 * @param tokenType
 * @returns {*}
 */
export const getToken = (tokenType = 'access_token') => {
  const token = store.getters['auth/token'];

  if (!hasProp(token, tokenType)) {
    throw Error(`Token type "${tokenType}" does not exist.`);
  }

  return token[tokenType];
};

/**
 * @description Get access query
 * @returns {string}
 */
export const getAccessQuery = () => `?access_token=${getToken()}`;
